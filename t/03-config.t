use v6;

use Test;
use Puma;
use Puma::Config;
use Puma::Build;

plan *;

my $cfg = Puma::Config.new;

is($cfg.stage, "{cwd}/blib", "checking staging location");
is($cfg.config, "{cwd}/blib/.config", "checking config location");

#lives_ok($cfg.write_config, "writing config");
$cfg.write_config();
ok($cfg.stage.IO ~~ :d, "blib presence");
ok($cfg.config.IO ~~ :s, "config presence");

my $fh = open($cfg.config, :r);
my $text = $fh.slurp;
$fh.close();

my $expectext = "install_dir[bin]: {$cfg.install_dir<bin>}\n"~
                "install_dir[doc]: {$cfg.install_dir<doc>}\n"~
                "install_dir[lib]: {$cfg.install_dir<lib>}\n"~
                "install_dir[sbin]: {$cfg.install_dir<sbin>}\n"~
                "stage: {$cfg.stage}\n"~
                "config: {$cfg.config}\n"~
                "stage_dir[bin]: {$cfg.stage_dir<bin>}\n"~
                "stage_dir[doc]: {$cfg.stage_dir<doc>}\n"~
                "stage_dir[lib]: {$cfg.stage_dir<lib>}\n"~
                "stage_dir[sbin]: {$cfg.stage_dir<sbin>}\n"~
                "config_dependencies: {$cfg.config_dependencies}\n"~
                "build_dependencies: {$cfg.build_dependencies}\n"~
                "run_dependencies: {$cfg.run_dependencies}\n"~
                "conflicts: {$cfg.conflicts}\n"~
                "recommends: {$cfg.recommends}\n";

is($text, $expectext, "checking config contents");

my $old_cfg = Puma::Config.new(bindir => "/usr/bin", docdir => "/usr/share/doc", libdir => "/usr/lib", sbindir => "/usr/sbin");
$old_cfg.write_config();

#lives_ok($cfg.read_config);
$cfg = Puma::Config.read_config();
isa_ok($cfg, Puma::Config, "testing config object");

is($cfg.install_dir<bin>, $old_cfg.install_dir<bin>, "reading install_dir<bin>");
is($cfg.install_dir<doc>, $old_cfg.install_dir<doc>, "reading install_dir<doc>");
is($cfg.install_dir<lib>, $old_cfg.install_dir<lib>, "reading install_dir<lib>");
is($cfg.install_dir<sbin>, $old_cfg.install_dir<sbin>, "reading install_dir<sbin>");
is($cfg.stage_dir<bin>, $old_cfg.stage_dir<bin>, "reading stage_dir<bin>");

# cleanup
unlink($cfg.config);
unlink($cfg.stage);

done;

# vim:ft=perl6:sw=4:ts=4:et
