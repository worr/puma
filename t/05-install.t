#!/usr/bin/env perl6

use v6;
use Test;

use Puma::Config;
use Puma::Build;
use Puma::Install;

plan *;

(my $cfg = Puma::Config.new()).write_config();
my $build = Puma::Build.new(:cfg($cfg.config));
my $install = Puma::Install.new(:cfg($cfg.config));
$build.setup_stage;
$build.copy_files;
$build.compile_libs;
$build.strip_docs;

#lives_ok($install.install);
$install.install("lib");

done;

# vim:ft=perl6:sw=4:ts=4:et
