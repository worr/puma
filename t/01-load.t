use v6;

use Test;
use Puma;
use Puma::Config;
use Puma::Build;
use Puma::Install;

plan *;

isa_ok(Puma.new(name => "test", version => 1.0), Puma);
isa_ok(Puma::Config.new(), Puma::Config);

(my $cfg = Puma::Config.new()).write_config();

isa_ok(Puma::Build.new(), Puma::Build);
isa_ok(Puma::Build.new($cfg.config), Puma::Build);
isa_ok(Puma::Build.new($cfg), Puma::Build);

isa_ok(Puma::Install.new, Puma::Install);
isa_ok(Puma::Install.new($cfg.config), Puma::Install);
isa_ok(Puma::Install.new($cfg), Puma::Install);

# name and version are required
#dies_ok(Puma.new());
#dies_ok(Puma.new(name => "test"));
#dies_ok(Puma.new(version => 1.0));

# cleanup
unlink($cfg.config);
unlink($cfg.stage);

done;

# vim: ft=perl6
