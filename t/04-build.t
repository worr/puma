use v6;

use Test;
use Puma;
use Puma::Config;
use Puma::Build;

plan *;

(my $cfg = Puma::Config.new).write_config();
my $build = Puma::Build.new(:cfg($cfg.config));

#lives_ok($build.setup_stage(), setting up staging directory);
$build.setup_stage();

ok($cfg.stage_dir<bin>.IO ~~ :d, "testing staged bindir");
ok($cfg.stage_dir<doc>.IO ~~ :d, "testing staged docdir");
ok($cfg.stage_dir<lib>.IO ~~ :d, "testing staged libdir");
ok($cfg.stage_dir<sbin>.IO ~~ :d, "testing staged sbindir");

#lives_ok($build.copy_files)
$build.copy_files();
my @files = dir("./lib");
my @moved = dir($cfg.stage_dir<lib>);
is(@moved.sort, @files.sort, "writing to staging");

#lives_ok($build.compile_libs());
$build.compile_libs();
ok("./blib/lib/Puma.pir".IO ~~ :s, "testing compilation");
ok("./blib/lib/Puma/Build.pir".IO ~~ :s, "testing compilation");
ok("./blib/lib/Puma/Config.pir".IO ~~ :s, "testing compilation");

#lives_ok($build.strip_docs());
$build.strip_docs();
ok("./blib/doc/Puma.3pm".IO ~~ :s, "testing doc stripping");

# cleanup
unlink($cfg.stage_dir<bin>);
unlink($cfg.stage_dir<doc>);
unlink($cfg.stage_dir<lib>);
unlink($cfg.stage_dir<sbin>);

done;

# vim:ft=perl6:sw=4:ts=4:et
