#!/usr/bin/env perl6
use v6;


use Puma::Config;
use Puma::FileList;

use File::Copy;
use File::Mkdir;

class Puma::Build {

    has Puma::Config $!cfg;
    has Puma::FileList $!filelist = Puma::FileList.new();

    multi method new() {
        return self.bless(*, :cfg(Puma::Config.new.read_config()));
    }

    multi method new(Str $config_loc) {
        return self.bless(*, :cfg(Puma::Config.new.read_config($config_loc)));
    }

    multi method new(Puma::Config $cfg) {
        return self.bless(*, :cfg($cfg.clone));
    }

    multi method setup_stage() {
        mkdir($!cfg.stage_dir<bin>, 0o755, :p);
        mkdir($!cfg.stage_dir<doc>, 0o755, :p);
        mkdir($!cfg.stage_dir<lib>, 0o755, :p);
        mkdir($!cfg.stage_dir<sbin>, 0o755, :p);
    }

    multi method copy_files() {
        for <./bin ./doc ./lib ./sbin> -> $pre {
            cp("$pre", "{$!cfg.stage}/$pre", :r);
        }
    }

    multi method compile_libs() {
        $.compile_libs($!cfg.stage_dir<lib>);
    }

    multi method compile_libs(Str $path) {
        my $full_path;
        my $pir;

        for dir($path) -> $file {
            $full_path = "$path/$file";
            $.compile_libs($full_path) if $full_path.IO ~~ :d;
            if $file ~~ m/( \.pm || \.pm6 || \.pl || \.pl6 )$/ {
                $pir = $full_path;
                $pir ~~ s/\..+?$/\.pir/;
                run("perl6 --target=pir $full_path > $pir");
            }
        }
    }

    multi method strip_docs() {
        my @libs = $!filelist.lib.list;
        for @libs -> $lib is copy {
            $lib ~~ m/(\.pm|\.pl)/;
            my $ext = $0;
            $lib ~~ s/\.pm|\.pl//;
            run("perldoc -oman -dblib/doc/{$lib}.3pm lib/$lib$ext");
        }
    }
}

# vim:ft=perl6:sw=4:ts=4:et
