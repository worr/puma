#!/usr/bin/env perl6

use v6;
use File::Tools;

class Puma::FileList {
    has Array $!_all;
    has Array $!_bin;
    has Array $!_doc;
    has Array $!_lib;
    has Array $!_sbin;

    method _get_files(*@dirs, :$prefix="") {
        my @ret;
        for @dirs -> $dir is copy {
            if $dir.IO ~~ :f { return $dir; }
            $dir ~= "/";
            for dir($dir) -> $file {
                @ret.push(self._get_files("$dir$file", prefix => "$dir"));
            }
        }

        return @ret;
    }

    method all() {
        return $!_all if $!_all;
        $!_all = Array.new(self.bin, self.doc, self.lib, self.sbin);
        return $!_all;
    }

    method bin() {
        return $!_bin if $!_bin;
        if "./bin".IO ~~ :d {
            $!_bin = Array.new(self._get_files("./bin")).map({ s/^\.\/\w+\///; });
        }

        return $!_bin;
    }

    method doc() {
        return $!_doc if $!_doc;
        if "./doc".IO ~~ :d {
            $!_doc = Array.new(self._get_files("./doc")).map({ s/^\.\/\w+\///; });
        }

        return $!_doc;
    }

    method lib() {
        return $!_lib if $!_lib;
        if "./lib".IO ~~ :d {
            $!_lib = Array.new(self._get_files("./lib")).map({ s/^\.\/\w+\///; });
        }

        return $!_lib;
    }

    method sbin() {
        return $!_sbin if $!_sbin;
        if "./sbin".IO ~~ :d {
            $!_sbin = Array.new(self._get_files("./sbin")).map({ s/^\.\/\w+\///; });
        }

        return $!_sbin;
    }
}

# vim:ft=perl6:sw=4:ts=4:et
