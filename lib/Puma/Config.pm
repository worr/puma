#!/usr/bin/env perl6
use v6;

use Puma;

class Puma::Config does Puma::Base {

# These MUST be overridable for distros like gentoo that
# build in a chroot and merge the files in

    has Hash $.install_dir = {
        bin => $*VM<config><bindir>,
        doc => $*VM<config><docdir>,
        lib => "$*VM<config><libdir>"~"$*VM<config><versiondir>/languages/perl6/lib",
        sbin => $*VM<config><sbindir>,
    };

    has Str $.stage = cwd~"/blib";
    has Str $.config = "$.stage/.config";

    has Hash $.stage_dir = { 
        bin => "$.stage/bin",
        doc => "$.stage/doc",
        lib => "$.stage/lib",
        sbin => "$.stage/sbin",
    };

    multi method write_config() {
        mkdir(self.stage, 0o755);

        my $fh = open(self.config, :w);
        for self.^attributes -> $i is copy {
            ($i = $i.Str) ~~ s/^\$\!//;
            if $i ~~ "install_dir"|"stage_dir" {
                for self."$i"().keys -> $key {
                    $fh.say("{$i}[$key]: { self."$i"(){$key}}");
                }
            } else {
                $fh.say("$i: {self."$i"()}");
            }
        }
        $fh.close();
    }

    multi method read_config($config = "./blib/.config") {
        if $config.IO !~~ :s {
            die("Cannot read config at $config");
        }

        my $fh = open($config, :r);
        my %params;

        for $fh.lines -> $line is copy {
            $line.trans("\{\}" => "");
            if $line ~~ m/(\w+)\[(\w+)\]:\s(.+)/ {
                %params{$0.Str}{$1.Str} = $2.Str;
            } elsif $line ~~ m/(\w+)\:\s(.+)/ {
                %params{$0.Str} = $1.Str;
            }
        }

        $fh.close();
        return Puma::Config.new(|%params);
    }
}

# vim:ft=perl6:sw=4:ts=4:et
