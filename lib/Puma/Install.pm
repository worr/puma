#!/usr/bin/env perl6

use v6;

use Puma::Config;
use Puma::FileList;

use File::Copy;

class Puma::Install {
    has Puma::Config $!cfg;
    has Puma::FileList $!file_list = Puma::FileList.new;
    has @!targets = ("bin", "lib", "sbin", "doc");

    multi method new() {
        return self.bless(*, :cfg(Puma::Config.new.read_config()));
    }

    multi method new(Str $config_loc) {
        return self.bless(*, :cfg(Puma::Config.new.read_config($config_loc)));
    }

    multi method new(Puma::Config $cfg) {
        return self.bless(*, :cfg($cfg.clone));
    }

    multi method install(Str *@targets) {
        for @targets -> $target {
            cp("blib/$target", "{$!cfg.install_dir{$target}}", :r);
        }
    }
}

# vim:ft=perl6:sw=4:ts=4:et
