#!/usr/bin/env perl6

=begin
=head1 Puma - a Perl6 module builder

This program builds perl6 modules!

=end

use v6;

role Puma::Base {
    has Hash $.config_dependencies;
    has Hash $.build_dependencies;
    has Hash $.run_dependencies;
    has Hash $.conflicts;
    has Hash $.recommends;
}

class Puma does Puma::Base {

    has Str $.name;
    has Real $.version;
    has Hash $.config_req;

    method new(Str :$name!, Real :$version!, 
                     Hash :$config_dependencies,
                     Hash :$build_dependencies,
                     Hash :$run_dependencies,
                     Hash :$recomments,
                     Hash :$conflicts ) {
        return self.bless(*, :$name, :$version, :$config_dependencies, 
                     :$build_dependencies,
                     :$run_dependencies, :$recomments, :$conflicts);
    }

    method run(Str $action) {
    }
}

# vim:ft=perl6:sw=4:ts=4:et
